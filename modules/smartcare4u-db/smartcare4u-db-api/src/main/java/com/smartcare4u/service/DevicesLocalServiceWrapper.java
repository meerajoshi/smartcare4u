/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.smartcare4u.service;

import com.liferay.portal.kernel.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link DevicesLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see DevicesLocalService
 * @generated
 */
public class DevicesLocalServiceWrapper
	implements DevicesLocalService, ServiceWrapper<DevicesLocalService> {

	public DevicesLocalServiceWrapper(DevicesLocalService devicesLocalService) {
		_devicesLocalService = devicesLocalService;
	}

	/**
	 * Adds the devices to the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect DevicesLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param devices the devices
	 * @return the devices that was added
	 */
	@Override
	public com.smartcare4u.model.Devices addDevices(
		com.smartcare4u.model.Devices devices) {

		return _devicesLocalService.addDevices(devices);
	}

	/**
	 * Creates a new devices with the primary key. Does not add the devices to the database.
	 *
	 * @param deviceId the primary key for the new devices
	 * @return the new devices
	 */
	@Override
	public com.smartcare4u.model.Devices createDevices(long deviceId) {
		return _devicesLocalService.createDevices(deviceId);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel createPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _devicesLocalService.createPersistedModel(primaryKeyObj);
	}

	/**
	 * Deletes the devices from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect DevicesLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param devices the devices
	 * @return the devices that was removed
	 */
	@Override
	public com.smartcare4u.model.Devices deleteDevices(
		com.smartcare4u.model.Devices devices) {

		return _devicesLocalService.deleteDevices(devices);
	}

	/**
	 * Deletes the devices with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect DevicesLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param deviceId the primary key of the devices
	 * @return the devices that was removed
	 * @throws PortalException if a devices with the primary key could not be found
	 */
	@Override
	public com.smartcare4u.model.Devices deleteDevices(long deviceId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _devicesLocalService.deleteDevices(deviceId);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel deletePersistedModel(
			com.liferay.portal.kernel.model.PersistedModel persistedModel)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _devicesLocalService.deletePersistedModel(persistedModel);
	}

	@Override
	public <T> T dslQuery(com.liferay.petra.sql.dsl.query.DSLQuery dslQuery) {
		return _devicesLocalService.dslQuery(dslQuery);
	}

	@Override
	public int dslQueryCount(
		com.liferay.petra.sql.dsl.query.DSLQuery dslQuery) {

		return _devicesLocalService.dslQueryCount(dslQuery);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _devicesLocalService.dynamicQuery();
	}

	/**
	 * Performs a dynamic query on the database and returns the matching rows.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _devicesLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	 * Performs a dynamic query on the database and returns a range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.smartcare4u.model.impl.DevicesModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @return the range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) {

		return _devicesLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * Performs a dynamic query on the database and returns an ordered range of the matching rows.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.smartcare4u.model.impl.DevicesModelImpl</code>.
	 * </p>
	 *
	 * @param dynamicQuery the dynamic query
	 * @param start the lower bound of the range of model instances
	 * @param end the upper bound of the range of model instances (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching rows
	 */
	@Override
	public <T> java.util.List<T> dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator<T> orderByComparator) {

		return _devicesLocalService.dynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery) {

		return _devicesLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	 * Returns the number of rows matching the dynamic query.
	 *
	 * @param dynamicQuery the dynamic query
	 * @param projection the projection to apply to the query
	 * @return the number of rows matching the dynamic query
	 */
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection) {

		return _devicesLocalService.dynamicQueryCount(dynamicQuery, projection);
	}

	@Override
	public com.smartcare4u.model.Devices fetchDevices(long deviceId) {
		return _devicesLocalService.fetchDevices(deviceId);
	}

	/**
	 * Returns the devices matching the UUID and group.
	 *
	 * @param uuid the devices's UUID
	 * @param groupId the primary key of the group
	 * @return the matching devices, or <code>null</code> if a matching devices could not be found
	 */
	@Override
	public com.smartcare4u.model.Devices fetchDevicesByUuidAndGroupId(
		String uuid, long groupId) {

		return _devicesLocalService.fetchDevicesByUuidAndGroupId(uuid, groupId);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.ActionableDynamicQuery
		getActionableDynamicQuery() {

		return _devicesLocalService.getActionableDynamicQuery();
	}

	/**
	 * Returns the devices with the primary key.
	 *
	 * @param deviceId the primary key of the devices
	 * @return the devices
	 * @throws PortalException if a devices with the primary key could not be found
	 */
	@Override
	public com.smartcare4u.model.Devices getDevices(long deviceId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _devicesLocalService.getDevices(deviceId);
	}

	/**
	 * Returns the devices matching the UUID and group.
	 *
	 * @param uuid the devices's UUID
	 * @param groupId the primary key of the group
	 * @return the matching devices
	 * @throws PortalException if a matching devices could not be found
	 */
	@Override
	public com.smartcare4u.model.Devices getDevicesByUuidAndGroupId(
			String uuid, long groupId)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _devicesLocalService.getDevicesByUuidAndGroupId(uuid, groupId);
	}

	/**
	 * Returns a range of all the deviceses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>com.smartcare4u.model.impl.DevicesModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of deviceses
	 * @param end the upper bound of the range of deviceses (not inclusive)
	 * @return the range of deviceses
	 */
	@Override
	public java.util.List<com.smartcare4u.model.Devices> getDeviceses(
		int start, int end) {

		return _devicesLocalService.getDeviceses(start, end);
	}

	/**
	 * Returns the number of deviceses.
	 *
	 * @return the number of deviceses
	 */
	@Override
	public int getDevicesesCount() {
		return _devicesLocalService.getDevicesesCount();
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.IndexableActionableDynamicQuery
		getIndexableActionableDynamicQuery() {

		return _devicesLocalService.getIndexableActionableDynamicQuery();
	}

	/**
	 * Returns the OSGi service identifier.
	 *
	 * @return the OSGi service identifier
	 */
	@Override
	public String getOSGiServiceIdentifier() {
		return _devicesLocalService.getOSGiServiceIdentifier();
	}

	@Override
	public java.util.List<com.smartcare4u.model.Devices>
			getPatientListByPatientId(long patientId)
		throws com.liferay.portal.kernel.exception.SystemException {

		return _devicesLocalService.getPatientListByPatientId(patientId);
	}

	/**
	 * @throws PortalException
	 */
	@Override
	public com.liferay.portal.kernel.model.PersistedModel getPersistedModel(
			java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException {

		return _devicesLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	 * Updates the devices in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	 *
	 * <p>
	 * <strong>Important:</strong> Inspect DevicesLocalServiceImpl for overloaded versions of the method. If provided, use these entry points to the API, as the implementation logic may require the additional parameters defined there.
	 * </p>
	 *
	 * @param devices the devices
	 * @return the devices that was updated
	 */
	@Override
	public com.smartcare4u.model.Devices updateDevices(
		com.smartcare4u.model.Devices devices) {

		return _devicesLocalService.updateDevices(devices);
	}

	@Override
	public DevicesLocalService getWrappedService() {
		return _devicesLocalService;
	}

	@Override
	public void setWrappedService(DevicesLocalService devicesLocalService) {
		_devicesLocalService = devicesLocalService;
	}

	private DevicesLocalService _devicesLocalService;

}