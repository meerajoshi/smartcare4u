/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.smartcare4u.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Brian Wing Shun Chan
 * @deprecated As of Athanasius (7.3.x), with no direct replacement
 * @generated
 */
@Deprecated
public class DevicesSoap implements Serializable {

	public static DevicesSoap toSoapModel(Devices model) {
		DevicesSoap soapModel = new DevicesSoap();

		soapModel.setUuid(model.getUuid());
		soapModel.setDeviceId(model.getDeviceId());
		soapModel.setDeviceUniqueId(model.getDeviceUniqueId());
		soapModel.setGroupId(model.getGroupId());
		soapModel.setOrgId(model.getOrgId());
		soapModel.setPatientId(model.getPatientId());
		soapModel.setImeiDevice(model.getImeiDevice());
		soapModel.setImeiSim(model.getImeiSim());
		soapModel.setSimProvider(model.getSimProvider());
		soapModel.setProduct(model.getProduct());
		soapModel.setInstallationDate(model.getInstallationDate());
		soapModel.setDeliveryDate(model.getDeliveryDate());
		soapModel.setReturnalDate(model.getReturnalDate());

		return soapModel;
	}

	public static DevicesSoap[] toSoapModels(Devices[] models) {
		DevicesSoap[] soapModels = new DevicesSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static DevicesSoap[][] toSoapModels(Devices[][] models) {
		DevicesSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new DevicesSoap[models.length][models[0].length];
		}
		else {
			soapModels = new DevicesSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static DevicesSoap[] toSoapModels(List<Devices> models) {
		List<DevicesSoap> soapModels = new ArrayList<DevicesSoap>(
			models.size());

		for (Devices model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new DevicesSoap[soapModels.size()]);
	}

	public DevicesSoap() {
	}

	public long getPrimaryKey() {
		return _deviceId;
	}

	public void setPrimaryKey(long pk) {
		setDeviceId(pk);
	}

	public String getUuid() {
		return _uuid;
	}

	public void setUuid(String uuid) {
		_uuid = uuid;
	}

	public long getDeviceId() {
		return _deviceId;
	}

	public void setDeviceId(long deviceId) {
		_deviceId = deviceId;
	}

	public String getDeviceUniqueId() {
		return _deviceUniqueId;
	}

	public void setDeviceUniqueId(String deviceUniqueId) {
		_deviceUniqueId = deviceUniqueId;
	}

	public long getGroupId() {
		return _groupId;
	}

	public void setGroupId(long groupId) {
		_groupId = groupId;
	}

	public long getOrgId() {
		return _orgId;
	}

	public void setOrgId(long orgId) {
		_orgId = orgId;
	}

	public long getPatientId() {
		return _patientId;
	}

	public void setPatientId(long patientId) {
		_patientId = patientId;
	}

	public String getImeiDevice() {
		return _imeiDevice;
	}

	public void setImeiDevice(String imeiDevice) {
		_imeiDevice = imeiDevice;
	}

	public String getImeiSim() {
		return _imeiSim;
	}

	public void setImeiSim(String imeiSim) {
		_imeiSim = imeiSim;
	}

	public String getSimProvider() {
		return _simProvider;
	}

	public void setSimProvider(String simProvider) {
		_simProvider = simProvider;
	}

	public String getProduct() {
		return _product;
	}

	public void setProduct(String product) {
		_product = product;
	}

	public Date getInstallationDate() {
		return _installationDate;
	}

	public void setInstallationDate(Date installationDate) {
		_installationDate = installationDate;
	}

	public Date getDeliveryDate() {
		return _deliveryDate;
	}

	public void setDeliveryDate(Date deliveryDate) {
		_deliveryDate = deliveryDate;
	}

	public Date getReturnalDate() {
		return _returnalDate;
	}

	public void setReturnalDate(Date returnalDate) {
		_returnalDate = returnalDate;
	}

	private String _uuid;
	private long _deviceId;
	private String _deviceUniqueId;
	private long _groupId;
	private long _orgId;
	private long _patientId;
	private String _imeiDevice;
	private String _imeiSim;
	private String _simProvider;
	private String _product;
	private Date _installationDate;
	private Date _deliveryDate;
	private Date _returnalDate;

}