/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.smartcare4u.service.persistence;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.OrderByComparator;

import com.smartcare4u.model.Devices;

import java.io.Serializable;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;
import org.osgi.util.tracker.ServiceTracker;

/**
 * The persistence utility for the devices service. This utility wraps <code>com.smartcare4u.service.persistence.impl.DevicesPersistenceImpl</code> and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see DevicesPersistence
 * @generated
 */
public class DevicesUtil {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#clearCache(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static void clearCache(Devices devices) {
		getPersistence().clearCache(devices);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public static long countWithDynamicQuery(DynamicQuery dynamicQuery) {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#fetchByPrimaryKeys(Set)
	 */
	public static Map<Serializable, Devices> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {

		return getPersistence().fetchByPrimaryKeys(primaryKeys);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Devices> findWithDynamicQuery(
		DynamicQuery dynamicQuery) {

		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Devices> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end) {

		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Devices> findWithDynamicQuery(
		DynamicQuery dynamicQuery, int start, int end,
		OrderByComparator<Devices> orderByComparator) {

		return getPersistence().findWithDynamicQuery(
			dynamicQuery, start, end, orderByComparator);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel)
	 */
	public static Devices update(Devices devices) {
		return getPersistence().update(devices);
	}

	/**
	 * @see com.liferay.portal.kernel.service.persistence.BasePersistence#update(com.liferay.portal.kernel.model.BaseModel, ServiceContext)
	 */
	public static Devices update(
		Devices devices, ServiceContext serviceContext) {

		return getPersistence().update(devices, serviceContext);
	}

	/**
	 * Returns all the deviceses where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching deviceses
	 */
	public static List<Devices> findByUuid(String uuid) {
		return getPersistence().findByUuid(uuid);
	}

	/**
	 * Returns a range of all the deviceses where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>DevicesModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of deviceses
	 * @param end the upper bound of the range of deviceses (not inclusive)
	 * @return the range of matching deviceses
	 */
	public static List<Devices> findByUuid(String uuid, int start, int end) {
		return getPersistence().findByUuid(uuid, start, end);
	}

	/**
	 * Returns an ordered range of all the deviceses where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>DevicesModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of deviceses
	 * @param end the upper bound of the range of deviceses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching deviceses
	 */
	public static List<Devices> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<Devices> orderByComparator) {

		return getPersistence().findByUuid(uuid, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the deviceses where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>DevicesModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of deviceses
	 * @param end the upper bound of the range of deviceses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching deviceses
	 */
	public static List<Devices> findByUuid(
		String uuid, int start, int end,
		OrderByComparator<Devices> orderByComparator, boolean useFinderCache) {

		return getPersistence().findByUuid(
			uuid, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first devices in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching devices
	 * @throws NoSuchDevicesException if a matching devices could not be found
	 */
	public static Devices findByUuid_First(
			String uuid, OrderByComparator<Devices> orderByComparator)
		throws com.smartcare4u.exception.NoSuchDevicesException {

		return getPersistence().findByUuid_First(uuid, orderByComparator);
	}

	/**
	 * Returns the first devices in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching devices, or <code>null</code> if a matching devices could not be found
	 */
	public static Devices fetchByUuid_First(
		String uuid, OrderByComparator<Devices> orderByComparator) {

		return getPersistence().fetchByUuid_First(uuid, orderByComparator);
	}

	/**
	 * Returns the last devices in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching devices
	 * @throws NoSuchDevicesException if a matching devices could not be found
	 */
	public static Devices findByUuid_Last(
			String uuid, OrderByComparator<Devices> orderByComparator)
		throws com.smartcare4u.exception.NoSuchDevicesException {

		return getPersistence().findByUuid_Last(uuid, orderByComparator);
	}

	/**
	 * Returns the last devices in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching devices, or <code>null</code> if a matching devices could not be found
	 */
	public static Devices fetchByUuid_Last(
		String uuid, OrderByComparator<Devices> orderByComparator) {

		return getPersistence().fetchByUuid_Last(uuid, orderByComparator);
	}

	/**
	 * Returns the deviceses before and after the current devices in the ordered set where uuid = &#63;.
	 *
	 * @param deviceId the primary key of the current devices
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next devices
	 * @throws NoSuchDevicesException if a devices with the primary key could not be found
	 */
	public static Devices[] findByUuid_PrevAndNext(
			long deviceId, String uuid,
			OrderByComparator<Devices> orderByComparator)
		throws com.smartcare4u.exception.NoSuchDevicesException {

		return getPersistence().findByUuid_PrevAndNext(
			deviceId, uuid, orderByComparator);
	}

	/**
	 * Removes all the deviceses where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public static void removeByUuid(String uuid) {
		getPersistence().removeByUuid(uuid);
	}

	/**
	 * Returns the number of deviceses where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching deviceses
	 */
	public static int countByUuid(String uuid) {
		return getPersistence().countByUuid(uuid);
	}

	/**
	 * Returns the devices where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchDevicesException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching devices
	 * @throws NoSuchDevicesException if a matching devices could not be found
	 */
	public static Devices findByUUID_G(String uuid, long groupId)
		throws com.smartcare4u.exception.NoSuchDevicesException {

		return getPersistence().findByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the devices where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching devices, or <code>null</code> if a matching devices could not be found
	 */
	public static Devices fetchByUUID_G(String uuid, long groupId) {
		return getPersistence().fetchByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the devices where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching devices, or <code>null</code> if a matching devices could not be found
	 */
	public static Devices fetchByUUID_G(
		String uuid, long groupId, boolean useFinderCache) {

		return getPersistence().fetchByUUID_G(uuid, groupId, useFinderCache);
	}

	/**
	 * Removes the devices where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the devices that was removed
	 */
	public static Devices removeByUUID_G(String uuid, long groupId)
		throws com.smartcare4u.exception.NoSuchDevicesException {

		return getPersistence().removeByUUID_G(uuid, groupId);
	}

	/**
	 * Returns the number of deviceses where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching deviceses
	 */
	public static int countByUUID_G(String uuid, long groupId) {
		return getPersistence().countByUUID_G(uuid, groupId);
	}

	/**
	 * Returns all the deviceses where patientId = &#63;.
	 *
	 * @param patientId the patient ID
	 * @return the matching deviceses
	 */
	public static List<Devices> findBygetPatientListByPatientId(
		long patientId) {

		return getPersistence().findBygetPatientListByPatientId(patientId);
	}

	/**
	 * Returns a range of all the deviceses where patientId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>DevicesModelImpl</code>.
	 * </p>
	 *
	 * @param patientId the patient ID
	 * @param start the lower bound of the range of deviceses
	 * @param end the upper bound of the range of deviceses (not inclusive)
	 * @return the range of matching deviceses
	 */
	public static List<Devices> findBygetPatientListByPatientId(
		long patientId, int start, int end) {

		return getPersistence().findBygetPatientListByPatientId(
			patientId, start, end);
	}

	/**
	 * Returns an ordered range of all the deviceses where patientId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>DevicesModelImpl</code>.
	 * </p>
	 *
	 * @param patientId the patient ID
	 * @param start the lower bound of the range of deviceses
	 * @param end the upper bound of the range of deviceses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching deviceses
	 */
	public static List<Devices> findBygetPatientListByPatientId(
		long patientId, int start, int end,
		OrderByComparator<Devices> orderByComparator) {

		return getPersistence().findBygetPatientListByPatientId(
			patientId, start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the deviceses where patientId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>DevicesModelImpl</code>.
	 * </p>
	 *
	 * @param patientId the patient ID
	 * @param start the lower bound of the range of deviceses
	 * @param end the upper bound of the range of deviceses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching deviceses
	 */
	public static List<Devices> findBygetPatientListByPatientId(
		long patientId, int start, int end,
		OrderByComparator<Devices> orderByComparator, boolean useFinderCache) {

		return getPersistence().findBygetPatientListByPatientId(
			patientId, start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Returns the first devices in the ordered set where patientId = &#63;.
	 *
	 * @param patientId the patient ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching devices
	 * @throws NoSuchDevicesException if a matching devices could not be found
	 */
	public static Devices findBygetPatientListByPatientId_First(
			long patientId, OrderByComparator<Devices> orderByComparator)
		throws com.smartcare4u.exception.NoSuchDevicesException {

		return getPersistence().findBygetPatientListByPatientId_First(
			patientId, orderByComparator);
	}

	/**
	 * Returns the first devices in the ordered set where patientId = &#63;.
	 *
	 * @param patientId the patient ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching devices, or <code>null</code> if a matching devices could not be found
	 */
	public static Devices fetchBygetPatientListByPatientId_First(
		long patientId, OrderByComparator<Devices> orderByComparator) {

		return getPersistence().fetchBygetPatientListByPatientId_First(
			patientId, orderByComparator);
	}

	/**
	 * Returns the last devices in the ordered set where patientId = &#63;.
	 *
	 * @param patientId the patient ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching devices
	 * @throws NoSuchDevicesException if a matching devices could not be found
	 */
	public static Devices findBygetPatientListByPatientId_Last(
			long patientId, OrderByComparator<Devices> orderByComparator)
		throws com.smartcare4u.exception.NoSuchDevicesException {

		return getPersistence().findBygetPatientListByPatientId_Last(
			patientId, orderByComparator);
	}

	/**
	 * Returns the last devices in the ordered set where patientId = &#63;.
	 *
	 * @param patientId the patient ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching devices, or <code>null</code> if a matching devices could not be found
	 */
	public static Devices fetchBygetPatientListByPatientId_Last(
		long patientId, OrderByComparator<Devices> orderByComparator) {

		return getPersistence().fetchBygetPatientListByPatientId_Last(
			patientId, orderByComparator);
	}

	/**
	 * Returns the deviceses before and after the current devices in the ordered set where patientId = &#63;.
	 *
	 * @param deviceId the primary key of the current devices
	 * @param patientId the patient ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next devices
	 * @throws NoSuchDevicesException if a devices with the primary key could not be found
	 */
	public static Devices[] findBygetPatientListByPatientId_PrevAndNext(
			long deviceId, long patientId,
			OrderByComparator<Devices> orderByComparator)
		throws com.smartcare4u.exception.NoSuchDevicesException {

		return getPersistence().findBygetPatientListByPatientId_PrevAndNext(
			deviceId, patientId, orderByComparator);
	}

	/**
	 * Removes all the deviceses where patientId = &#63; from the database.
	 *
	 * @param patientId the patient ID
	 */
	public static void removeBygetPatientListByPatientId(long patientId) {
		getPersistence().removeBygetPatientListByPatientId(patientId);
	}

	/**
	 * Returns the number of deviceses where patientId = &#63;.
	 *
	 * @param patientId the patient ID
	 * @return the number of matching deviceses
	 */
	public static int countBygetPatientListByPatientId(long patientId) {
		return getPersistence().countBygetPatientListByPatientId(patientId);
	}

	/**
	 * Caches the devices in the entity cache if it is enabled.
	 *
	 * @param devices the devices
	 */
	public static void cacheResult(Devices devices) {
		getPersistence().cacheResult(devices);
	}

	/**
	 * Caches the deviceses in the entity cache if it is enabled.
	 *
	 * @param deviceses the deviceses
	 */
	public static void cacheResult(List<Devices> deviceses) {
		getPersistence().cacheResult(deviceses);
	}

	/**
	 * Creates a new devices with the primary key. Does not add the devices to the database.
	 *
	 * @param deviceId the primary key for the new devices
	 * @return the new devices
	 */
	public static Devices create(long deviceId) {
		return getPersistence().create(deviceId);
	}

	/**
	 * Removes the devices with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param deviceId the primary key of the devices
	 * @return the devices that was removed
	 * @throws NoSuchDevicesException if a devices with the primary key could not be found
	 */
	public static Devices remove(long deviceId)
		throws com.smartcare4u.exception.NoSuchDevicesException {

		return getPersistence().remove(deviceId);
	}

	public static Devices updateImpl(Devices devices) {
		return getPersistence().updateImpl(devices);
	}

	/**
	 * Returns the devices with the primary key or throws a <code>NoSuchDevicesException</code> if it could not be found.
	 *
	 * @param deviceId the primary key of the devices
	 * @return the devices
	 * @throws NoSuchDevicesException if a devices with the primary key could not be found
	 */
	public static Devices findByPrimaryKey(long deviceId)
		throws com.smartcare4u.exception.NoSuchDevicesException {

		return getPersistence().findByPrimaryKey(deviceId);
	}

	/**
	 * Returns the devices with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param deviceId the primary key of the devices
	 * @return the devices, or <code>null</code> if a devices with the primary key could not be found
	 */
	public static Devices fetchByPrimaryKey(long deviceId) {
		return getPersistence().fetchByPrimaryKey(deviceId);
	}

	/**
	 * Returns all the deviceses.
	 *
	 * @return the deviceses
	 */
	public static List<Devices> findAll() {
		return getPersistence().findAll();
	}

	/**
	 * Returns a range of all the deviceses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>DevicesModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of deviceses
	 * @param end the upper bound of the range of deviceses (not inclusive)
	 * @return the range of deviceses
	 */
	public static List<Devices> findAll(int start, int end) {
		return getPersistence().findAll(start, end);
	}

	/**
	 * Returns an ordered range of all the deviceses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>DevicesModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of deviceses
	 * @param end the upper bound of the range of deviceses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of deviceses
	 */
	public static List<Devices> findAll(
		int start, int end, OrderByComparator<Devices> orderByComparator) {

		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	 * Returns an ordered range of all the deviceses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>DevicesModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of deviceses
	 * @param end the upper bound of the range of deviceses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of deviceses
	 */
	public static List<Devices> findAll(
		int start, int end, OrderByComparator<Devices> orderByComparator,
		boolean useFinderCache) {

		return getPersistence().findAll(
			start, end, orderByComparator, useFinderCache);
	}

	/**
	 * Removes all the deviceses from the database.
	 */
	public static void removeAll() {
		getPersistence().removeAll();
	}

	/**
	 * Returns the number of deviceses.
	 *
	 * @return the number of deviceses
	 */
	public static int countAll() {
		return getPersistence().countAll();
	}

	public static DevicesPersistence getPersistence() {
		return _serviceTracker.getService();
	}

	private static ServiceTracker<DevicesPersistence, DevicesPersistence>
		_serviceTracker;

	static {
		Bundle bundle = FrameworkUtil.getBundle(DevicesPersistence.class);

		ServiceTracker<DevicesPersistence, DevicesPersistence> serviceTracker =
			new ServiceTracker<DevicesPersistence, DevicesPersistence>(
				bundle.getBundleContext(), DevicesPersistence.class, null);

		serviceTracker.open();

		_serviceTracker = serviceTracker;
	}

}