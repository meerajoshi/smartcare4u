/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.smartcare4u.model;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.model.wrapper.BaseModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Devices}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Devices
 * @generated
 */
public class DevicesWrapper
	extends BaseModelWrapper<Devices>
	implements Devices, ModelWrapper<Devices> {

	public DevicesWrapper(Devices devices) {
		super(devices);
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("uuid", getUuid());
		attributes.put("deviceId", getDeviceId());
		attributes.put("deviceUniqueId", getDeviceUniqueId());
		attributes.put("groupId", getGroupId());
		attributes.put("orgId", getOrgId());
		attributes.put("patientId", getPatientId());
		attributes.put("imeiDevice", getImeiDevice());
		attributes.put("imeiSim", getImeiSim());
		attributes.put("simProvider", getSimProvider());
		attributes.put("product", getProduct());
		attributes.put("installationDate", getInstallationDate());
		attributes.put("deliveryDate", getDeliveryDate());
		attributes.put("returnalDate", getReturnalDate());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		String uuid = (String)attributes.get("uuid");

		if (uuid != null) {
			setUuid(uuid);
		}

		Long deviceId = (Long)attributes.get("deviceId");

		if (deviceId != null) {
			setDeviceId(deviceId);
		}

		String deviceUniqueId = (String)attributes.get("deviceUniqueId");

		if (deviceUniqueId != null) {
			setDeviceUniqueId(deviceUniqueId);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		Long orgId = (Long)attributes.get("orgId");

		if (orgId != null) {
			setOrgId(orgId);
		}

		Long patientId = (Long)attributes.get("patientId");

		if (patientId != null) {
			setPatientId(patientId);
		}

		String imeiDevice = (String)attributes.get("imeiDevice");

		if (imeiDevice != null) {
			setImeiDevice(imeiDevice);
		}

		String imeiSim = (String)attributes.get("imeiSim");

		if (imeiSim != null) {
			setImeiSim(imeiSim);
		}

		String simProvider = (String)attributes.get("simProvider");

		if (simProvider != null) {
			setSimProvider(simProvider);
		}

		String product = (String)attributes.get("product");

		if (product != null) {
			setProduct(product);
		}

		Date installationDate = (Date)attributes.get("installationDate");

		if (installationDate != null) {
			setInstallationDate(installationDate);
		}

		Date deliveryDate = (Date)attributes.get("deliveryDate");

		if (deliveryDate != null) {
			setDeliveryDate(deliveryDate);
		}

		Date returnalDate = (Date)attributes.get("returnalDate");

		if (returnalDate != null) {
			setReturnalDate(returnalDate);
		}
	}

	/**
	 * Returns the delivery date of this devices.
	 *
	 * @return the delivery date of this devices
	 */
	@Override
	public Date getDeliveryDate() {
		return model.getDeliveryDate();
	}

	/**
	 * Returns the device ID of this devices.
	 *
	 * @return the device ID of this devices
	 */
	@Override
	public long getDeviceId() {
		return model.getDeviceId();
	}

	/**
	 * Returns the device unique ID of this devices.
	 *
	 * @return the device unique ID of this devices
	 */
	@Override
	public String getDeviceUniqueId() {
		return model.getDeviceUniqueId();
	}

	/**
	 * Returns the group ID of this devices.
	 *
	 * @return the group ID of this devices
	 */
	@Override
	public long getGroupId() {
		return model.getGroupId();
	}

	/**
	 * Returns the imei device of this devices.
	 *
	 * @return the imei device of this devices
	 */
	@Override
	public String getImeiDevice() {
		return model.getImeiDevice();
	}

	/**
	 * Returns the imei sim of this devices.
	 *
	 * @return the imei sim of this devices
	 */
	@Override
	public String getImeiSim() {
		return model.getImeiSim();
	}

	/**
	 * Returns the installation date of this devices.
	 *
	 * @return the installation date of this devices
	 */
	@Override
	public Date getInstallationDate() {
		return model.getInstallationDate();
	}

	/**
	 * Returns the org ID of this devices.
	 *
	 * @return the org ID of this devices
	 */
	@Override
	public long getOrgId() {
		return model.getOrgId();
	}

	/**
	 * Returns the patient ID of this devices.
	 *
	 * @return the patient ID of this devices
	 */
	@Override
	public long getPatientId() {
		return model.getPatientId();
	}

	/**
	 * Returns the primary key of this devices.
	 *
	 * @return the primary key of this devices
	 */
	@Override
	public long getPrimaryKey() {
		return model.getPrimaryKey();
	}

	/**
	 * Returns the product of this devices.
	 *
	 * @return the product of this devices
	 */
	@Override
	public String getProduct() {
		return model.getProduct();
	}

	/**
	 * Returns the returnal date of this devices.
	 *
	 * @return the returnal date of this devices
	 */
	@Override
	public Date getReturnalDate() {
		return model.getReturnalDate();
	}

	/**
	 * Returns the sim provider of this devices.
	 *
	 * @return the sim provider of this devices
	 */
	@Override
	public String getSimProvider() {
		return model.getSimProvider();
	}

	/**
	 * Returns the uuid of this devices.
	 *
	 * @return the uuid of this devices
	 */
	@Override
	public String getUuid() {
		return model.getUuid();
	}

	@Override
	public void persist() {
		model.persist();
	}

	/**
	 * Sets the delivery date of this devices.
	 *
	 * @param deliveryDate the delivery date of this devices
	 */
	@Override
	public void setDeliveryDate(Date deliveryDate) {
		model.setDeliveryDate(deliveryDate);
	}

	/**
	 * Sets the device ID of this devices.
	 *
	 * @param deviceId the device ID of this devices
	 */
	@Override
	public void setDeviceId(long deviceId) {
		model.setDeviceId(deviceId);
	}

	/**
	 * Sets the device unique ID of this devices.
	 *
	 * @param deviceUniqueId the device unique ID of this devices
	 */
	@Override
	public void setDeviceUniqueId(String deviceUniqueId) {
		model.setDeviceUniqueId(deviceUniqueId);
	}

	/**
	 * Sets the group ID of this devices.
	 *
	 * @param groupId the group ID of this devices
	 */
	@Override
	public void setGroupId(long groupId) {
		model.setGroupId(groupId);
	}

	/**
	 * Sets the imei device of this devices.
	 *
	 * @param imeiDevice the imei device of this devices
	 */
	@Override
	public void setImeiDevice(String imeiDevice) {
		model.setImeiDevice(imeiDevice);
	}

	/**
	 * Sets the imei sim of this devices.
	 *
	 * @param imeiSim the imei sim of this devices
	 */
	@Override
	public void setImeiSim(String imeiSim) {
		model.setImeiSim(imeiSim);
	}

	/**
	 * Sets the installation date of this devices.
	 *
	 * @param installationDate the installation date of this devices
	 */
	@Override
	public void setInstallationDate(Date installationDate) {
		model.setInstallationDate(installationDate);
	}

	/**
	 * Sets the org ID of this devices.
	 *
	 * @param orgId the org ID of this devices
	 */
	@Override
	public void setOrgId(long orgId) {
		model.setOrgId(orgId);
	}

	/**
	 * Sets the patient ID of this devices.
	 *
	 * @param patientId the patient ID of this devices
	 */
	@Override
	public void setPatientId(long patientId) {
		model.setPatientId(patientId);
	}

	/**
	 * Sets the primary key of this devices.
	 *
	 * @param primaryKey the primary key of this devices
	 */
	@Override
	public void setPrimaryKey(long primaryKey) {
		model.setPrimaryKey(primaryKey);
	}

	/**
	 * Sets the product of this devices.
	 *
	 * @param product the product of this devices
	 */
	@Override
	public void setProduct(String product) {
		model.setProduct(product);
	}

	/**
	 * Sets the returnal date of this devices.
	 *
	 * @param returnalDate the returnal date of this devices
	 */
	@Override
	public void setReturnalDate(Date returnalDate) {
		model.setReturnalDate(returnalDate);
	}

	/**
	 * Sets the sim provider of this devices.
	 *
	 * @param simProvider the sim provider of this devices
	 */
	@Override
	public void setSimProvider(String simProvider) {
		model.setSimProvider(simProvider);
	}

	/**
	 * Sets the uuid of this devices.
	 *
	 * @param uuid the uuid of this devices
	 */
	@Override
	public void setUuid(String uuid) {
		model.setUuid(uuid);
	}

	@Override
	protected DevicesWrapper wrap(Devices devices) {
		return new DevicesWrapper(devices);
	}

}