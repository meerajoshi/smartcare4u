/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.smartcare4u.service.persistence;

import com.liferay.portal.kernel.service.persistence.BasePersistence;

import com.smartcare4u.exception.NoSuchDevicesException;
import com.smartcare4u.model.Devices;

import org.osgi.annotation.versioning.ProviderType;

/**
 * The persistence interface for the devices service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see DevicesUtil
 * @generated
 */
@ProviderType
public interface DevicesPersistence extends BasePersistence<Devices> {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link DevicesUtil} to access the devices persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	 * Returns all the deviceses where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the matching deviceses
	 */
	public java.util.List<Devices> findByUuid(String uuid);

	/**
	 * Returns a range of all the deviceses where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>DevicesModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of deviceses
	 * @param end the upper bound of the range of deviceses (not inclusive)
	 * @return the range of matching deviceses
	 */
	public java.util.List<Devices> findByUuid(String uuid, int start, int end);

	/**
	 * Returns an ordered range of all the deviceses where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>DevicesModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of deviceses
	 * @param end the upper bound of the range of deviceses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching deviceses
	 */
	public java.util.List<Devices> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Devices>
			orderByComparator);

	/**
	 * Returns an ordered range of all the deviceses where uuid = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>DevicesModelImpl</code>.
	 * </p>
	 *
	 * @param uuid the uuid
	 * @param start the lower bound of the range of deviceses
	 * @param end the upper bound of the range of deviceses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching deviceses
	 */
	public java.util.List<Devices> findByUuid(
		String uuid, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Devices>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first devices in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching devices
	 * @throws NoSuchDevicesException if a matching devices could not be found
	 */
	public Devices findByUuid_First(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<Devices>
				orderByComparator)
		throws NoSuchDevicesException;

	/**
	 * Returns the first devices in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching devices, or <code>null</code> if a matching devices could not be found
	 */
	public Devices fetchByUuid_First(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Devices>
			orderByComparator);

	/**
	 * Returns the last devices in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching devices
	 * @throws NoSuchDevicesException if a matching devices could not be found
	 */
	public Devices findByUuid_Last(
			String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<Devices>
				orderByComparator)
		throws NoSuchDevicesException;

	/**
	 * Returns the last devices in the ordered set where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching devices, or <code>null</code> if a matching devices could not be found
	 */
	public Devices fetchByUuid_Last(
		String uuid,
		com.liferay.portal.kernel.util.OrderByComparator<Devices>
			orderByComparator);

	/**
	 * Returns the deviceses before and after the current devices in the ordered set where uuid = &#63;.
	 *
	 * @param deviceId the primary key of the current devices
	 * @param uuid the uuid
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next devices
	 * @throws NoSuchDevicesException if a devices with the primary key could not be found
	 */
	public Devices[] findByUuid_PrevAndNext(
			long deviceId, String uuid,
			com.liferay.portal.kernel.util.OrderByComparator<Devices>
				orderByComparator)
		throws NoSuchDevicesException;

	/**
	 * Removes all the deviceses where uuid = &#63; from the database.
	 *
	 * @param uuid the uuid
	 */
	public void removeByUuid(String uuid);

	/**
	 * Returns the number of deviceses where uuid = &#63;.
	 *
	 * @param uuid the uuid
	 * @return the number of matching deviceses
	 */
	public int countByUuid(String uuid);

	/**
	 * Returns the devices where uuid = &#63; and groupId = &#63; or throws a <code>NoSuchDevicesException</code> if it could not be found.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching devices
	 * @throws NoSuchDevicesException if a matching devices could not be found
	 */
	public Devices findByUUID_G(String uuid, long groupId)
		throws NoSuchDevicesException;

	/**
	 * Returns the devices where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the matching devices, or <code>null</code> if a matching devices could not be found
	 */
	public Devices fetchByUUID_G(String uuid, long groupId);

	/**
	 * Returns the devices where uuid = &#63; and groupId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @param useFinderCache whether to use the finder cache
	 * @return the matching devices, or <code>null</code> if a matching devices could not be found
	 */
	public Devices fetchByUUID_G(
		String uuid, long groupId, boolean useFinderCache);

	/**
	 * Removes the devices where uuid = &#63; and groupId = &#63; from the database.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the devices that was removed
	 */
	public Devices removeByUUID_G(String uuid, long groupId)
		throws NoSuchDevicesException;

	/**
	 * Returns the number of deviceses where uuid = &#63; and groupId = &#63;.
	 *
	 * @param uuid the uuid
	 * @param groupId the group ID
	 * @return the number of matching deviceses
	 */
	public int countByUUID_G(String uuid, long groupId);

	/**
	 * Returns all the deviceses where patientId = &#63;.
	 *
	 * @param patientId the patient ID
	 * @return the matching deviceses
	 */
	public java.util.List<Devices> findBygetPatientListByPatientId(
		long patientId);

	/**
	 * Returns a range of all the deviceses where patientId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>DevicesModelImpl</code>.
	 * </p>
	 *
	 * @param patientId the patient ID
	 * @param start the lower bound of the range of deviceses
	 * @param end the upper bound of the range of deviceses (not inclusive)
	 * @return the range of matching deviceses
	 */
	public java.util.List<Devices> findBygetPatientListByPatientId(
		long patientId, int start, int end);

	/**
	 * Returns an ordered range of all the deviceses where patientId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>DevicesModelImpl</code>.
	 * </p>
	 *
	 * @param patientId the patient ID
	 * @param start the lower bound of the range of deviceses
	 * @param end the upper bound of the range of deviceses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching deviceses
	 */
	public java.util.List<Devices> findBygetPatientListByPatientId(
		long patientId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Devices>
			orderByComparator);

	/**
	 * Returns an ordered range of all the deviceses where patientId = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>DevicesModelImpl</code>.
	 * </p>
	 *
	 * @param patientId the patient ID
	 * @param start the lower bound of the range of deviceses
	 * @param end the upper bound of the range of deviceses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of matching deviceses
	 */
	public java.util.List<Devices> findBygetPatientListByPatientId(
		long patientId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Devices>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Returns the first devices in the ordered set where patientId = &#63;.
	 *
	 * @param patientId the patient ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching devices
	 * @throws NoSuchDevicesException if a matching devices could not be found
	 */
	public Devices findBygetPatientListByPatientId_First(
			long patientId,
			com.liferay.portal.kernel.util.OrderByComparator<Devices>
				orderByComparator)
		throws NoSuchDevicesException;

	/**
	 * Returns the first devices in the ordered set where patientId = &#63;.
	 *
	 * @param patientId the patient ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching devices, or <code>null</code> if a matching devices could not be found
	 */
	public Devices fetchBygetPatientListByPatientId_First(
		long patientId,
		com.liferay.portal.kernel.util.OrderByComparator<Devices>
			orderByComparator);

	/**
	 * Returns the last devices in the ordered set where patientId = &#63;.
	 *
	 * @param patientId the patient ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching devices
	 * @throws NoSuchDevicesException if a matching devices could not be found
	 */
	public Devices findBygetPatientListByPatientId_Last(
			long patientId,
			com.liferay.portal.kernel.util.OrderByComparator<Devices>
				orderByComparator)
		throws NoSuchDevicesException;

	/**
	 * Returns the last devices in the ordered set where patientId = &#63;.
	 *
	 * @param patientId the patient ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching devices, or <code>null</code> if a matching devices could not be found
	 */
	public Devices fetchBygetPatientListByPatientId_Last(
		long patientId,
		com.liferay.portal.kernel.util.OrderByComparator<Devices>
			orderByComparator);

	/**
	 * Returns the deviceses before and after the current devices in the ordered set where patientId = &#63;.
	 *
	 * @param deviceId the primary key of the current devices
	 * @param patientId the patient ID
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next devices
	 * @throws NoSuchDevicesException if a devices with the primary key could not be found
	 */
	public Devices[] findBygetPatientListByPatientId_PrevAndNext(
			long deviceId, long patientId,
			com.liferay.portal.kernel.util.OrderByComparator<Devices>
				orderByComparator)
		throws NoSuchDevicesException;

	/**
	 * Removes all the deviceses where patientId = &#63; from the database.
	 *
	 * @param patientId the patient ID
	 */
	public void removeBygetPatientListByPatientId(long patientId);

	/**
	 * Returns the number of deviceses where patientId = &#63;.
	 *
	 * @param patientId the patient ID
	 * @return the number of matching deviceses
	 */
	public int countBygetPatientListByPatientId(long patientId);

	/**
	 * Caches the devices in the entity cache if it is enabled.
	 *
	 * @param devices the devices
	 */
	public void cacheResult(Devices devices);

	/**
	 * Caches the deviceses in the entity cache if it is enabled.
	 *
	 * @param deviceses the deviceses
	 */
	public void cacheResult(java.util.List<Devices> deviceses);

	/**
	 * Creates a new devices with the primary key. Does not add the devices to the database.
	 *
	 * @param deviceId the primary key for the new devices
	 * @return the new devices
	 */
	public Devices create(long deviceId);

	/**
	 * Removes the devices with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param deviceId the primary key of the devices
	 * @return the devices that was removed
	 * @throws NoSuchDevicesException if a devices with the primary key could not be found
	 */
	public Devices remove(long deviceId) throws NoSuchDevicesException;

	public Devices updateImpl(Devices devices);

	/**
	 * Returns the devices with the primary key or throws a <code>NoSuchDevicesException</code> if it could not be found.
	 *
	 * @param deviceId the primary key of the devices
	 * @return the devices
	 * @throws NoSuchDevicesException if a devices with the primary key could not be found
	 */
	public Devices findByPrimaryKey(long deviceId)
		throws NoSuchDevicesException;

	/**
	 * Returns the devices with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param deviceId the primary key of the devices
	 * @return the devices, or <code>null</code> if a devices with the primary key could not be found
	 */
	public Devices fetchByPrimaryKey(long deviceId);

	/**
	 * Returns all the deviceses.
	 *
	 * @return the deviceses
	 */
	public java.util.List<Devices> findAll();

	/**
	 * Returns a range of all the deviceses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>DevicesModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of deviceses
	 * @param end the upper bound of the range of deviceses (not inclusive)
	 * @return the range of deviceses
	 */
	public java.util.List<Devices> findAll(int start, int end);

	/**
	 * Returns an ordered range of all the deviceses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>DevicesModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of deviceses
	 * @param end the upper bound of the range of deviceses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of deviceses
	 */
	public java.util.List<Devices> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Devices>
			orderByComparator);

	/**
	 * Returns an ordered range of all the deviceses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to <code>QueryUtil#ALL_POS</code> will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent, then the query will include the default ORDER BY logic from <code>DevicesModelImpl</code>.
	 * </p>
	 *
	 * @param start the lower bound of the range of deviceses
	 * @param end the upper bound of the range of deviceses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param useFinderCache whether to use the finder cache
	 * @return the ordered range of deviceses
	 */
	public java.util.List<Devices> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator<Devices>
			orderByComparator,
		boolean useFinderCache);

	/**
	 * Removes all the deviceses from the database.
	 */
	public void removeAll();

	/**
	 * Returns the number of deviceses.
	 *
	 * @return the number of deviceses
	 */
	public int countAll();

}