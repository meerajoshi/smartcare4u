/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.smartcare4u.model;

import com.liferay.petra.sql.dsl.Column;
import com.liferay.petra.sql.dsl.base.BaseTable;

import java.sql.Types;

import java.util.Date;

/**
 * The table class for the &quot;smartcare_Devices&quot; database table.
 *
 * @author Brian Wing Shun Chan
 * @see Devices
 * @generated
 */
public class DevicesTable extends BaseTable<DevicesTable> {

	public static final DevicesTable INSTANCE = new DevicesTable();

	public final Column<DevicesTable, String> uuid = createColumn(
		"uuid_", String.class, Types.VARCHAR, Column.FLAG_DEFAULT);
	public final Column<DevicesTable, Long> deviceId = createColumn(
		"deviceId", Long.class, Types.BIGINT, Column.FLAG_PRIMARY);
	public final Column<DevicesTable, String> deviceUniqueId = createColumn(
		"deviceUniqueId", String.class, Types.VARCHAR, Column.FLAG_DEFAULT);
	public final Column<DevicesTable, Long> groupId = createColumn(
		"groupId", Long.class, Types.BIGINT, Column.FLAG_DEFAULT);
	public final Column<DevicesTable, Long> orgId = createColumn(
		"orgId", Long.class, Types.BIGINT, Column.FLAG_DEFAULT);
	public final Column<DevicesTable, Long> patientId = createColumn(
		"patientId", Long.class, Types.BIGINT, Column.FLAG_DEFAULT);
	public final Column<DevicesTable, String> imeiDevice = createColumn(
		"imeiDevice", String.class, Types.VARCHAR, Column.FLAG_DEFAULT);
	public final Column<DevicesTable, String> imeiSim = createColumn(
		"imeiSim", String.class, Types.VARCHAR, Column.FLAG_DEFAULT);
	public final Column<DevicesTable, String> simProvider = createColumn(
		"simProvider", String.class, Types.VARCHAR, Column.FLAG_DEFAULT);
	public final Column<DevicesTable, String> product = createColumn(
		"product", String.class, Types.VARCHAR, Column.FLAG_DEFAULT);
	public final Column<DevicesTable, Date> installationDate = createColumn(
		"installationDate", Date.class, Types.TIMESTAMP, Column.FLAG_DEFAULT);
	public final Column<DevicesTable, Date> deliveryDate = createColumn(
		"deliveryDate", Date.class, Types.TIMESTAMP, Column.FLAG_DEFAULT);
	public final Column<DevicesTable, Date> returnalDate = createColumn(
		"returnalDate", Date.class, Types.TIMESTAMP, Column.FLAG_DEFAULT);

	private DevicesTable() {
		super("smartcare_Devices", DevicesTable::new);
	}

}