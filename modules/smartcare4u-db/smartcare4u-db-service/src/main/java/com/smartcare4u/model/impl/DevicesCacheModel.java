/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.smartcare4u.model.impl;

import com.liferay.petra.lang.HashUtil;
import com.liferay.petra.string.StringBundler;
import com.liferay.portal.kernel.model.CacheModel;

import com.smartcare4u.model.Devices;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Devices in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
public class DevicesCacheModel implements CacheModel<Devices>, Externalizable {

	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (!(object instanceof DevicesCacheModel)) {
			return false;
		}

		DevicesCacheModel devicesCacheModel = (DevicesCacheModel)object;

		if (deviceId == devicesCacheModel.deviceId) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, deviceId);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(27);

		sb.append("{uuid=");
		sb.append(uuid);
		sb.append(", deviceId=");
		sb.append(deviceId);
		sb.append(", deviceUniqueId=");
		sb.append(deviceUniqueId);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append(", orgId=");
		sb.append(orgId);
		sb.append(", patientId=");
		sb.append(patientId);
		sb.append(", imeiDevice=");
		sb.append(imeiDevice);
		sb.append(", imeiSim=");
		sb.append(imeiSim);
		sb.append(", simProvider=");
		sb.append(simProvider);
		sb.append(", product=");
		sb.append(product);
		sb.append(", installationDate=");
		sb.append(installationDate);
		sb.append(", deliveryDate=");
		sb.append(deliveryDate);
		sb.append(", returnalDate=");
		sb.append(returnalDate);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public Devices toEntityModel() {
		DevicesImpl devicesImpl = new DevicesImpl();

		if (uuid == null) {
			devicesImpl.setUuid("");
		}
		else {
			devicesImpl.setUuid(uuid);
		}

		devicesImpl.setDeviceId(deviceId);

		if (deviceUniqueId == null) {
			devicesImpl.setDeviceUniqueId("");
		}
		else {
			devicesImpl.setDeviceUniqueId(deviceUniqueId);
		}

		devicesImpl.setGroupId(groupId);
		devicesImpl.setOrgId(orgId);
		devicesImpl.setPatientId(patientId);

		if (imeiDevice == null) {
			devicesImpl.setImeiDevice("");
		}
		else {
			devicesImpl.setImeiDevice(imeiDevice);
		}

		if (imeiSim == null) {
			devicesImpl.setImeiSim("");
		}
		else {
			devicesImpl.setImeiSim(imeiSim);
		}

		if (simProvider == null) {
			devicesImpl.setSimProvider("");
		}
		else {
			devicesImpl.setSimProvider(simProvider);
		}

		if (product == null) {
			devicesImpl.setProduct("");
		}
		else {
			devicesImpl.setProduct(product);
		}

		if (installationDate == Long.MIN_VALUE) {
			devicesImpl.setInstallationDate(null);
		}
		else {
			devicesImpl.setInstallationDate(new Date(installationDate));
		}

		if (deliveryDate == Long.MIN_VALUE) {
			devicesImpl.setDeliveryDate(null);
		}
		else {
			devicesImpl.setDeliveryDate(new Date(deliveryDate));
		}

		if (returnalDate == Long.MIN_VALUE) {
			devicesImpl.setReturnalDate(null);
		}
		else {
			devicesImpl.setReturnalDate(new Date(returnalDate));
		}

		devicesImpl.resetOriginalValues();

		return devicesImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		uuid = objectInput.readUTF();

		deviceId = objectInput.readLong();
		deviceUniqueId = objectInput.readUTF();

		groupId = objectInput.readLong();

		orgId = objectInput.readLong();

		patientId = objectInput.readLong();
		imeiDevice = objectInput.readUTF();
		imeiSim = objectInput.readUTF();
		simProvider = objectInput.readUTF();
		product = objectInput.readUTF();
		installationDate = objectInput.readLong();
		deliveryDate = objectInput.readLong();
		returnalDate = objectInput.readLong();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput) throws IOException {
		if (uuid == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(uuid);
		}

		objectOutput.writeLong(deviceId);

		if (deviceUniqueId == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(deviceUniqueId);
		}

		objectOutput.writeLong(groupId);

		objectOutput.writeLong(orgId);

		objectOutput.writeLong(patientId);

		if (imeiDevice == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(imeiDevice);
		}

		if (imeiSim == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(imeiSim);
		}

		if (simProvider == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(simProvider);
		}

		if (product == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(product);
		}

		objectOutput.writeLong(installationDate);
		objectOutput.writeLong(deliveryDate);
		objectOutput.writeLong(returnalDate);
	}

	public String uuid;
	public long deviceId;
	public String deviceUniqueId;
	public long groupId;
	public long orgId;
	public long patientId;
	public String imeiDevice;
	public String imeiSim;
	public String simProvider;
	public String product;
	public long installationDate;
	public long deliveryDate;
	public long returnalDate;

}