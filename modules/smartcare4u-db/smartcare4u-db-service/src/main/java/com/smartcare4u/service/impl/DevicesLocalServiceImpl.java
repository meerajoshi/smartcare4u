/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.smartcare4u.service.impl;

import com.liferay.portal.aop.AopService;
import com.smartcare4u.model.Devices;
import com.smartcare4u.service.base.DevicesLocalServiceBaseImpl;
import com.smartcare4u.service.persistence.DevicesUtil;

import org.osgi.service.component.annotations.Component;

/**
 * The implementation of the devices local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the <code>com.smartcare4u.service.DevicesLocalService</code> interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see DevicesLocalServiceBaseImpl
 */
@Component(
	property = "model.class.name=com.smartcare4u.model.Devices",
	service = AopService.class
)
public class DevicesLocalServiceImpl extends DevicesLocalServiceBaseImpl {
	public  java.util.List<Devices>
	getPatientListByPatientId(
	long patientId)
	throws com.liferay.portal.kernel.exception.SystemException {
	return DevicesUtil.findBygetPatientListByPatientId(patientId);
	}
	/*
	 * NOTE FOR DEVELOPERS:
	 * 
	 * smartcare4u-db-service
	 *
	 * Never reference this class directly. Use <code>com.smartcare4u.service.DevicesLocalService</code> via injection or a <code>org.osgi.util.tracker.ServiceTracker</code> or use <code>com.smartcare4u.service.DevicesLocalServiceUtil</code>.
	 */
}