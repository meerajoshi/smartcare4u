create index IX_8A6A7C52 on smartcare_Devices (patientId);
create unique index IX_D1EDDD14 on smartcare_Devices (uuid_[$COLUMN_LENGTH:75$], groupId);