create table smartcare_Devices (
	uuid_ VARCHAR(75) null,
	deviceId LONG not null primary key,
	deviceUniqueId VARCHAR(75) null,
	groupId LONG,
	orgId LONG,
	patientId LONG,
	imeiDevice VARCHAR(75) null,
	imeiSim VARCHAR(75) null,
	simProvider VARCHAR(75) null,
	product VARCHAR(75) null,
	installationDate DATE null,
	deliveryDate DATE null,
	returnalDate DATE null
);