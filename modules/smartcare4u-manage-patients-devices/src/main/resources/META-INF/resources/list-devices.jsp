<%@ include file="/init.jsp" %>
<nav class="navbar navbar-info bg-info justify-content-between mt-2">
  <b><a class="navbar-brand text-white"><liferay-ui:message key="devices" /></a></b>
  <form class="form-inline">
    <a href="${url}${patientId}" class="btn btn-light text-primary my-2 my-sm-0" type="submit"><liferay-ui:message key="devices.add" /></a>
  </form>
</nav>
<table class="table">
    <tr >
        <th><liferay-ui:message key="devices.imei-device" /></th>
        <th><liferay-ui:message key="devices.imei-sim" /></th>
        <th><liferay-ui:message key="devices.sim-provider" /></th>
        <th><liferay-ui:message key="devices.product" /></th>
        <th><liferay-ui:message key="devices.unique-id" /></th>
        <th><liferay-ui:message key="devices.ins-date" /></th>
        <th><liferay-ui:message key="devices.delivery-date" /></th>
        <th><liferay-ui:message key="devices.returnal-date" /></th>
        <th><liferay-ui:message key="devices.grafana" /></th>
    </tr>
    <c:forEach items="${deviceList}" var="device">
    <tr>
    	<td>${device.getImeiDevice()}</td>
    	<td>${device.getImeiSim()}</td>
    	<td>${device.getSimProvider()}</td>
    	<td>${device.getProduct()}</td>
    	<td>${device.getDeviceUniqueId()}</td>
    	<td>${device.getInstallationDate()}</td>
    	<td>${device.getDeliveryDate()}</td>
    	<td>${device.getReturnalDate()}</td>
      <td><a href="https://lifedata.smartcare4u.ch/d/9l9WfyM7k/bio-data?orgId=1&refresh=5s&var-MyDevice=${device.getDeviceUniqueId()}" target="_blank"><liferay-ui:message key="devices.trace-device" />${device.getDeviceUniqueId()}</a></td>
    </tr> 
    </c:forEach>
  </table>