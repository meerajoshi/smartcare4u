<%@ include file="/init.jsp" %>
<portlet:actionURL name="/manage-devices/add-device" var="addDeviceURL"/>

<nav class="navbar navbar-info bg-info justify-content-between mt-2">
  <b><a class="navbar-brand text-white"><liferay-ui:message key="devices.add" /></a></b>
</nav>
<br/>

<div class=" portlet-content-container ml-3 mr-3">
<form action="${addDeviceURL}" name="deviceDataForm" method="POST">
  <input type="hidden" name="patientId" value="${patientId}">
  <div class="form-group">
    <label for="imeiDevice"><liferay-ui:message key="devices.imei-device" /></label>
    <input type="text" class="form-control" id="imeiDevice" name="imeiDevice" placeholder="Enter Device IMEI">
  </div>
  <div class="form-group">
    <label for="imeiSim"><liferay-ui:message key="devices.imei-sim" /></label>
    <input type="text" class="form-control" id="imeiSim" name="imeiSim" placeholder="Enter Sim IMEI">
  </div>
  <div class="form-group">
    <label for="simProvider"><liferay-ui:message key="ddevices.sim-provider" /></label>
    <input type="text" class="form-control" id="simProvider" name="simProvider" placeholder="Enter Sim Provider">
  </div>
  <div class="form-group">
    <label for="product"><liferay-ui:message key="devices.product" /></label>
    <input type="text" class="form-control" id="product" name="product" placeholder="Enter Product">
  </div>
  <div class="form-group">
    <label for="installationDate"><liferay-ui:message key="devices.ins-date" /></label>
    <input type="date" required="true" class="form-control" id="installationDate" name="installationDate">
  </div>
  <div class="form-group">
    <label for="deliveryDate"><liferay-ui:message key="devices.delivery-date" /></label>
    <input type="date" required="true" class="form-control" id="deliveryDate" name="deliveryDate">
  </div>
  <div class="form-group">
    <label for="returnalDate"><liferay-ui:message key="devices.returnal-date" /></label>
    <input type="date" required="true" class="form-control" id="returnalDate" name="returnalDate">
  </div>
  <div class="form-group">
    <label for="uDeviceId"><liferay-ui:message key="devices.grafana" /></label>
    <input type="text" required="true" class="form-control" id="uDeviceId" name="uDeviceId" placeholder="Enter Device IMEI">
  </div>
  <button type="submit" class="btn btn-primary"><liferay-ui:message key="submit" /></button>
</form>
</div>
  