<%@ include file="/init.jsp"%>

<nav class="navbar navbar-info bg-info justify-content-between mt-2">
	<b> 
		<a class="navbar-brand text-white">
			<liferay-ui:message key="patients" />
		</a>
	</b>
</nav>

<table class="table">
	<tr>
		<th><liferay-ui:message key="patient.name" /></th>
		<th><liferay-ui:message key="patient.email" /></th>
		<th><liferay-ui:message key="patient.screen-name" /></th>
		<th><liferay-ui:message key="patient.join-date" /></th>
		<th><liferay-ui:message key="patients.trace-device" /></th>
	</tr>
	<c:forEach items="${patientList}" var="patient">
		<tr>
			<td>
				<h4>
					<a style="color: black;"
						href='${url}${patient.getUserId()}' target="_self">
						${patient.getFullName()}
					</a>
				</h4>
			</td>
			<td>${patient.getEmailAddress()}</td>
			<td>${patient.getScreenName()}</td>
			<td>${patient.getCreateDate()}</td>
			<td><a href="${url}${patient.getUserId()}"><liferay-ui:message key="devices" /></a></td>
		</tr>
	</c:forEach>
</table>

