package com.smartcare4u.portal.devices.portlet;

import java.util.List;

import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.servlet.http.HttpServletRequest;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.smartcare4u.model.Devices;
import com.smartcare4u.portal.devices.constants.DevicesConstants;
import com.smartcare4u.portal.util.PatientsDevicesUtil;
import com.smartcare4u.service.DevicesLocalService;

/**
 * @author ignek_infotech
 */
@Component(immediate = true, property = { "com.liferay.portlet.display-category=SmartCare4u",
		"com.liferay.portlet.header-portlet-css=/css/main.css", "com.liferay.portlet.instanceable=false",
		"javax.portlet.display-name=Devices", "javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/list-devices.jsp", "javax.portlet.name=" + DevicesConstants.DEVICES,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user" }, service = Portlet.class)
public class DevicesListPortlet extends MVCPortlet {

	private static final Log log = LogFactoryUtil.getLog(DevicesListPortlet.class.getName());

	@Reference
	UserLocalService userLocalService;

	@Reference
	DevicesLocalService deviceLocalService;

	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse)
			throws java.io.IOException, PortletException {

		ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
		User currentUser = themeDisplay.getUser();

		HttpServletRequest origrequest = PortalUtil
				.getOriginalServletRequest(PortalUtil.getHttpServletRequest(renderRequest));

		String uid = origrequest.getParameter("patientId");
		

		long loggedInUserOrgId = 0;
		try {
			long[] orgIds = currentUser.getOrganizationIds();
			if (orgIds.length > 0) {
				loggedInUserOrgId = orgIds[0];
			}
		} catch (PortalException e) {
			log.error(e);
		}

		List<Devices> deviceList = deviceLocalService.getPatientListByPatientId(Long.parseLong(uid));
		
		renderRequest.setAttribute("deviceList", deviceList);
		renderRequest.setAttribute("patientId", uid);
		renderRequest.setAttribute("url", PatientsDevicesUtil.getPageURL(themeDisplay, "/add-device?patientId="));

		super.render(renderRequest, renderResponse);
	}
}