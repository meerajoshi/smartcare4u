package com.smartcare4u.portal.devices.action;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.servlet.http.HttpServletRequest;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.counter.kernel.service.CounterLocalServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCActionCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCActionCommand;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.smartcare4u.model.Devices;
import com.smartcare4u.portal.devices.constants.DevicesConstants;
import com.smartcare4u.portal.devices.portlet.AddDevicePortlet;
import com.smartcare4u.portal.util.PatientsDevicesUtil;
import com.smartcare4u.service.DevicesLocalService;

/**
 * @author ignek_infotech
 */
@Component(immediate = true, property = { 
		"javax.portlet.name=" + DevicesConstants.ADD_DEVICES,
		"mvc.command.name=/manage-devices/add-device" 
		}, 
	service = MVCActionCommand.class
)
public class AddDevicesAction extends BaseMVCActionCommand{
	private static final Log log = LogFactoryUtil.getLog(AddDevicePortlet.class.getName());

	@Reference
	UserLocalService userLocalService;

	@Reference
	DevicesLocalService deviceLocalService;
	
	protected void doProcessAction(ActionRequest actionRequest, ActionResponse actionResponse)
			throws ParseException, IOException, PortletException {
		
		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
		User currentUser = themeDisplay.getUser();

		long[] orgIds;
		long orgId = 0;
		try {
			orgIds = currentUser.getOrganizationIds();
			if (orgIds.length > 0) {
				orgId = orgIds[0];
			}
		} catch (PortalException e) {
			log.error(e);

		}
		long newDeviceId = CounterLocalServiceUtil.increment(Devices.class.getName());
		Devices device = deviceLocalService.createDevices(newDeviceId);
		HttpServletRequest origrequest = PortalUtil
				.getOriginalServletRequest(PortalUtil.getHttpServletRequest(actionRequest));

		long patientId = Long.parseLong(origrequest.getParameter("patientId"));

		String imeiDevice = origrequest.getParameter("imeiDevice");
		String imeiSim = origrequest.getParameter("imeiSim");
		String simProvider = origrequest.getParameter("simProvider");
		String product = origrequest.getParameter("product");
		String uDeviceID = origrequest.getParameter("uDeviceId");
		String d1 = origrequest.getParameter("installationDate");
		String d2 = origrequest.getParameter("deliveryDate");
		String d3 = origrequest.getParameter("returnalDate");
		Date installationDate = new SimpleDateFormat("dd-MM-yyyy").parse(d1);
		Date deliveryDate = new SimpleDateFormat("dd-MM-yyyy").parse(d2);
		Date returnalDate = new SimpleDateFormat("dd-MM-yyyy").parse(d3);


		device.setImeiSim(imeiSim);
		device.setSimProvider(simProvider);
		device.setProduct(product);
		device.setInstallationDate(installationDate);
		device.setDeliveryDate(deliveryDate);
		device.setReturnalDate(returnalDate);
		device.setDeviceUniqueId(uDeviceID);
		// device.setGroupId(groupId);
		device.setOrgId(orgId);
		device.setPatientId(patientId);
		device.setImeiDevice(imeiDevice);

		deviceLocalService.addDevices(device);
		log.info("------------------ Device Added--------------");
		try {
			actionResponse.sendRedirect(PatientsDevicesUtil.getPageURL(themeDisplay, "/device?patientId=") + patientId);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
}
