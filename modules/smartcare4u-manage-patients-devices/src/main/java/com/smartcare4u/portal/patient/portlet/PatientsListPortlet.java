package com.smartcare4u.portal.patient.portlet;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.Organization;
import com.liferay.portal.kernel.model.Role;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.WebKeys;
import com.smartcare4u.portal.patient.constants.PatientConstants;
import com.smartcare4u.portal.util.PatientsDevicesUtil;

/**
 * @author ignek_infotech
 */
@Component(immediate = true, property = { "com.liferay.portlet.display-category=SmartCare4u",
		"com.liferay.portlet.header-portlet-css=/css/main.css", "com.liferay.portlet.instanceable=false",
		"javax.portlet.display-name=Patient List Portlet", "javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/list-patients.jsp",
		"javax.portlet.name=" + PatientConstants.LIST_PATIENTS, "javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user" }, service = Portlet.class)
public class PatientsListPortlet extends MVCPortlet {

	private static final Log log = LogFactoryUtil.getLog(PatientsListPortlet.class.getName());

	@Reference
	UserLocalService userLocalService;

	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse)
			throws java.io.IOException, PortletException {

		ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);

		User currentUser = themeDisplay.getUser();

		long loggedInUserOrgId = 0;
		try {
			long[] orgIds = currentUser.getOrganizationIds();
			List<Organization> orgs = currentUser.getOrganizations();

			if (orgIds.length > 0) {
				loggedInUserOrgId = orgIds[0];
			}

		} catch (PortalException e) {
			log.error(e);
		}

		List<User> users = userLocalService.getOrganizationUsers(loggedInUserOrgId);
		List<User> patientList = new ArrayList<User>(users);

		for (Iterator iterator = patientList.iterator(); iterator.hasNext();) {
			User user = (User) iterator.next();
			List<Role> userRoles = user.getRoles();
			List<String> userRolesStr = new ArrayList<String>();

			for (Role userRole : userRoles) {
				userRolesStr.add(userRole.getName());
			}
			if (userRolesStr.contains("Org Admin")) {
				iterator.remove();
			}
		}

		renderRequest.setAttribute("patientList", patientList);
		renderRequest.setAttribute("url", PatientsDevicesUtil.getPageURL(themeDisplay, "/device?patientId="));
		super.render(renderRequest, renderResponse);
	}
}