package com.smartcare4u.portal.devices.constants;
/**
 * @author ignek_infotech
 */
public class DevicesConstants {
	public static final String DEVICES =
			"com_smartcare4u_portal_devices_ListPatientsPortlet";
	
	public static final String ADD_DEVICES =
			"com_smartcare4u_portal_devices_AddDevicePortlet";

}
