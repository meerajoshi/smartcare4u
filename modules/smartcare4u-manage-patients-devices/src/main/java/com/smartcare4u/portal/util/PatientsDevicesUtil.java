package com.smartcare4u.portal.util;

import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.PortalUtil;

public class PatientsDevicesUtil {

	public static String getPageURL(ThemeDisplay themeDisplay, String url) {

		StringBuilder sb = new StringBuilder();

		sb.append(themeDisplay.getPortalURL());
		if (themeDisplay.getLayout().isPrivateLayout()) {
			sb.append(PortalUtil.getPathFriendlyURLPrivateGroup());
		} else {
			sb.append(PortalUtil.getPathFriendlyURLPublic());
		}

		sb.append(themeDisplay.getScopeGroup().getFriendlyURL());
		sb.append(url);

		return sb.toString();
	}

}
